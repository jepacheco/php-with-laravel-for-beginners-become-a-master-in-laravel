<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    //

    public $directory = "/images/";

    use SoftDeletes;

    // Esto solo se coloca cuando el nombre de tu clase es distinto al nombre de la tabla en la BD al igual si la PK de esa tabla es diferente de 'id'
    // protected $table ='posts';
    // protected $primaryKey = 'post_id';

    protected $dates = ['delete_at'];


    protected $fillable = ['title','content','user_id','path'];


    public function user(){

        return $this->belongsTo('App\User');
    }

    public function photos(){

        return $this->morphMany('App\Photo', 'imageable');

    }

    public function tags(){
        return $this->morphToMany('App\Tag', 'taggable');
    }

    public static function scopeLastest($query){

        return $query-> orderBy('id', 'desc')->get();

    }

    public function getPathAttribute($value){

        return $this->directory . $value;

    }


}
