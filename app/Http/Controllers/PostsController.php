<?php

namespace App\Http\Controllers;

use App\Post; 
use Illuminate\Http\Request;
use App\Http\Requests\CreatePostRequest;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Para ver la coleccion completa de datos se usa el dd()
        //dd($post= Post::all());

        //Para ordenar el resultado de la busqueda
        //$posts= Post::orderBy('id', 'desc')->get();
        $posts= Post::lastest();

       return view('posts.index', compact('posts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts.create');
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        //

        $input = $request->all();

        if($file = $request->file('file')){
            $name = $file->getClientOriginalName();
            $file->move('images', $name);
            $input['path'] = $name;
        }

        Post::create($input);

        // $file = $request->file('file');
        // echo "<br>";
        // echo $file->getClientOriginalName();
        // echo "<br>";
        // echo $file->getClientSize();

        //return $request->get('title');

        //Post::create($request->all());

        // $input = $request->all();
        //
        //$input['title'] = $request->title;
        //
        //Post::create($request->all());

        // $this->validate($request, [
        //
        //     'title'=>'required',
        //     'content'=>'required'
        // ]);



        // $post = new Post;
        //
        // $post->title = $request->title;
        // $post->user_id = $request->user_id;
        // $post->content = $request->content;
        //
        // $post->save();
        //
        // return redirect('/posts');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post= Post::findOrFail($id);

        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $post= Post::findOrFail($id);

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $post = Post::findOrFail($id); 
        
        $post->update($request->all());

        return redirect('/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $post = Post::whereId($id);

        $post->delete();

        return redirect('/posts');


    }

    public function contact(){

        //$people = ['Edwin', 'Jose', 'James', 'Peter', 'Maria'];

        $people = ['xfgbzfb'];

        return view('contact', compact('people'));

    }

    public function show_post($id, $name, $password){

        // return view('post')->with('id',$id);

        return view('post', \compact('id', 'name', 'password'));

    }
}
