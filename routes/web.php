<?php

//para poder usar todo lo que tiene la Clase Post que al ser hijo de Model tiene muchas funciones
use App\Post;
use App\User;
use App\Country;
use App\Photo;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Route::get('/post/{id}', 'PostsController@index');

// Route::resource('posts', 'PostsController');

// Route::get('/contact', 'PostsController@contact');

// Route::get('post/{id}/{name}/{password}', 'PostsController@show_post');

//-------------------------DB sql queries--------------------------------------------

// Route::get('/insert', function () {

//      DB::insert('insert into posts (title,content) values (?, ?)', ['laravel is awesome with Pache', 'laravel is the best thing ever to PHP']);
    
// });

// Route::get('/read', function () {

//     $result = DB::select('select * from posts where id = ?', [1]);

//     //return $result;
//     //para mostrar el query

//     return var_dump($result);
//     //el "var_dumb" muestra con mayor detalle las columnas de la consulta

//     //Muestra solo el resultado
//     // foreach($result as $post)
//     // return $post->title;
    
// });

// Route::get('/update', function () {

//     $updated = DB::update('update posts set title = "Update title" where id = ?', [1]);

//     return $updated;
    
// });

// Route::get('/delete', function () {

//    $deleted = DB::delete('delete from posts where id = ?', [1]);

//     return $deleted;
    
// });

//------------------------------ELOQUENT--------------------------------------------------------

// Route::get('/read', function () {

//     $posts = Post::all();

//     foreach($posts as $post){
//         return $post->title;
//     }
    
// });

// Route::get('/find', function () {

//     $post = Post::find(2);
//     return $post->content;

//     // foreach($posts as $post){
//     //     return $post->title;
//     // }
    
// });

// Route::get('/findwhere', function () {

//     $posts = Post::where('id', 3)->orderBy('id','desc')->take(1)->get();

//     return $posts;
    
// });

// Route::get('/findmore', function () {

//     // $posts = Post::findOrFail(1);

//     // return $posts;

//     $posts = Post::where('id', '<', 50)->firstOrFail();
 
// });

// Route::get('/basicinsert', function () {

//     $post = new Post;

//     $post->title = 'New Eloquent title insert';
//     $post->content = 'Wow eloquent is really cool, look at this content';

//     $post->save();
    
// });

// Route::get('/basicinsert2', function () {

//     $post = Post::find(2);

//     $post->title = 'New Eloquent title insert 2';
//     $post->content = 'Wow eloquent is really cool, look at this content 2';

//     $post->save();
    
// });

// Route::get('/create', function () {

//     Post::create(['title'=>'the create method', 'content'=>'WOW I am learning a lot']);
    
// });

// Route::get('/update', function () {

//     Post::where('id', 2)->where('is_admin', 0)->update(['title'=> 'New PHP TITLE', 'content'=>'this is the Route update function']);
    
// });

// Route::get('/delete', function () {

//     $post = Post::find(2);

//     $post->delete();
// });

// Route::get('/delete2', function () {

//     Post::destroy([3,4]);

// //    Post::where('is_admin', 0)->delete();
    
// });

//  Route::get('/softdelete', function () {

//     Post::find(3)->delete();
     
//  });

//  Route::get('/readsoftdelete', function () {

//     // $post = Post::find(1);

//     // return $post;

//     $post = Post::withTrashed()->where('is_admin', 0)->get();

//     return $post;

//     // $post = Post::onlyTrashed()->where('is_admin', 0)->get();

//     // return $post;
     
//  });

// Route::get('/restore', function () {

//     Post::withTrashed()->where('is_admin', 0)->restore();
    
// });

// Route::get('/forcedelete', function () {

//     Post::withTrashed()->where('is_admin', 0)->forceDelete();
    
// });

//borrar solo las de estatus borrado
// Route::get('/forcedelete', function () {

//     Post::onlyTrashed()->where('is_admin', 0)->forceDelete();
    
// });

//----------------------ELOQUENT Relationships---------------------------------

// //One to One relationship
// Route::get('/user/{id}/post', function ($id) {

//     return User::find($id)->post->title;

    
// });

// Route::get('/post/{id}/user', function ($id) {

//     return Post::find($id)->user->name;
    
// });

// //One to Many relationship
// Route::get('/posts', function () {

//     $user = User::find(1);

//     foreach($user->posts as $post){
//         echo $post->title . "<br>";
//     }
    
// });

// //Many to Many relationship
// Route::get('/user/{id}/role', function ($id) {

//     $user = User::find($id)->roles()->orderBy('id', 'desc')->get();

//     return $user;

//     // foreach($user->roles as $role){
//     //     return $role->name;
//     // }
    
// });

// Accesing the intermediate table / pivot

// Route::get('user/pivot', function () {

//     $user = User::find(1);

//     foreach($user->roles as $role){
//         echo $role->pivot->created_at;
//     }
    
// });

// Route::get('/user/country', function () {

//     $country = Country::find(4);

//     foreach($country->posts as $post){
//         return $post->title;
//     }
    
// });

//pOLYMORPHIC rELATIONS

// Route::get('post/photos', function () {
    
//     $post = Post::find(1);

//     foreach($post->photos as $photo){

//         echo $photo->path . "<br>";

//     }

// });


// Route::get('post/{$id}/photos', function ($id) {
    
//     $post = Post::find($id);

//     foreach($post->photos as $photo){

//         echo $photo->path . "<br>";

//     }

// });


// Route::get('photo/{id}/post', function ($id) {

//     $photo= Photo::findOrFail($id);

//     return $photo->imageable;
    
// });

// Route::get('/post/tag', function () {

//     $post = Post::find(1);

//     foreach($post->tags as $tag){

//         echo $tag->name;
        
//     }
    
// });

//---------------------------------Crud Applcation--------------

Route::group(['middleware'=>'web'], function(){

    Route::resource('/posts', 'PostsController');

    Route::get('/dates', function () {

        $date = new DateTime('+1 week');

        echo $date->format('m-d-Y');

        echo '<br>';

        echo Carbon::now()->addDays(10)->diffForHumans();

        echo '<br>';

        echo Carbon::now()->subMonths(5)->diffForHumans();

        echo '<br>';

        echo Carbon::now()->yesterday()->diffForHumans();
        
    });

    Route::get('/getname', function () {

        $user = User::find(1);

        echo $user->name;
        
    });

    Route::get('/setname', function () {

        $user = User::find(1);

        $user->name = "Jesus Pacheco";

        $user->save();
        
    });

});