@extends('layouts.app')



@section('content')

  <h1>Create Post</h1>

  {{-- <form method="POST" action="/posts"> --}}

    {!! Form::open(['method'=>'POST', 'action'=>'PostsController@store', 'files'=>true]) !!}

    <div class="form-group">

      {!! Form::label('title', 'Title:') !!}
      {!! Form::text('title', null, ['class'=>'form-control']) !!}

      {!! Form::label('content', 'Content:') !!}
      {!! Form::text('content', null, ['class'=>'form-control']) !!}

      {!! Form::label('user_id', 'User_id:') !!}
      {!! Form::text('user_id', null, ['class'=>'form-control']) !!}

      {{csrf_field()}}

    </div>

    <div class="form-group">
      {!! Form::file('file', ['class'=>'form-control']) !!}
    </div>


    {{-- <div class="form-group">

      {!! Form::label('title', 'Title:') !!}
      {!! Form::text('title', null, ['class'=>'form-control']) !!}

      {!! Form::label('user_id', 'User_id:') !!}
      {!! Form::text('user_id', null, ['class'=>'form-control']) !!}

      {!! Form::label('content', 'Content:') !!}
      {!! Form::text('content', null, ['class'=>'form-control']) !!}

    </div> --}}

    {{-- <label for="">Title: </label>
    <input type="text" name="title" placeholder="Enter title">
    <input type="text" name="user_id" placeholder="Enter userID">
    <input type="text" name="content" placeholder="Enter content"> --}}

    

    <div class="form-group">

      {!! Form::submit('Create Post', ['class'=>'btn btn-primary']) !!}

    </div>


    {{-- <input type="submit" name="submit"> --}}

  {!! Form::close() !!}

  @if (count($errors) > 0)

    <div class="alert alert-danger">

      <ul>

        @foreach ($errors->all() as $error)

        <li>{{$error}}</li>
            
        @endforeach


      </ul>

    </div>
      
  @endif




@section('footer')





